﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            persona p1 = new persona();
            profesor pr1 = new profesor();

            Console.WriteLine("----\nPersona\n----");
            p1.Definir();
            p1.Responsabilidad();


            Console.WriteLine("----\nProfesor\n----");
            pr1.Definir();
            pr1.DefinirSueldo();
            pr1.Responsabilidad();
            pr1.Mostrarsueldo();
            

            Console.ReadKey();

        }
    }
    /*Crear una clase Persona que tenga como atributos el 
     * "cedula, nombre, apellido y la edad 
     * (definir las propiedades para poder acceder a dichos atributos)". 
     * Definir como responsabilidad un método para mostrar ó imprimir. 
     * Crear una segunda clase Profesor que herede de la clase Persona. 
     * Añadir un atributo sueldo ( y su propiedad) y el método para imprimir 
     * su sueldo. Definir un objeto de la clase Persona y llamar 
     * a sus métodos y propiedades. También crear un objeto de la clase
     * Profesor y llamar a sus métodos y propiedades.*/

    class persona
    {
        private long cedula;
        long Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }

        private string nombre;
        string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;
        string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private short edad;
        short Edad
        {
            get { return edad; }
            set { edad = value; }
        }

        public void Definir()
        {
            Console.WriteLine("Definir los atributos de la persona\n");

            Console.WriteLine("Definir cedula");
            Cedula = long.Parse(Console.ReadLine());

            Console.WriteLine("Definir nombre");
            Nombre = Console.ReadLine();
            
            Console.WriteLine("Definir apellido");
            Apellido = Console.ReadLine();

            Console.WriteLine("Definir edad");
            Edad = short.Parse(Console.ReadLine());
        }

        public void Responsabilidad()
        {
            Console.WriteLine("Cedula: " + Cedula);
            Console.WriteLine("Nombre: " + Nombre);
            Console.WriteLine("Apellido: " + Apellido);
            Console.WriteLine("Edad: " + Edad);
        }
    }

    class profesor : persona
    {
        private float sueldo;
        float Sueldo
        {
            get { return sueldo; }
            set { sueldo = value; }
        }

        public void DefinirSueldo()
        {
            Console.WriteLine("Defina el sueldo");
            Sueldo = float.Parse(Console.ReadLine());
        }

        public void Mostrarsueldo()
        {
            Console.WriteLine("Sueldo: " + Sueldo);
        }
    }
}
