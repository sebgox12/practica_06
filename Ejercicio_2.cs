﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class ProbarContacto
    {
        static void Main(string[] args)
        {
            short r = 1;
            while (r == 1)
            {
                Console.WriteLine("--------------------------------------");
                Console.WriteLine("ingrese los datos del primer contacto\n");

                Console.WriteLine("ingrese el nombre del contacto");
                string nombre = Console.ReadLine();

                Console.WriteLine("Ingrese el apellido del contacto");
                string apellido = Console.ReadLine();

                Console.WriteLine("Ingrese el numero de telefono");
                string numero = Console.ReadLine();

                Console.WriteLine("Ingrese la direccion del contacto");
                string direccion = Console.ReadLine();
                Console.WriteLine("--------------------------------------");

                contacto con1 = new contacto();

                con1.SetContacto(numero, apellido, numero, direccion);

                Console.Clear();

                Console.WriteLine("--------------------------------------");
                Console.WriteLine("ingrese los datos del segundo contacton\n");

                Console.WriteLine("ingrese el nombre del contacto");
                string nombre2 = Console.ReadLine();

                Console.WriteLine("Ingrese el apellido del contacto");
                string apellido2 = Console.ReadLine();

                Console.WriteLine("Ingrese el numero de telefono");
                string numero2 = Console.ReadLine();

                Console.WriteLine("Ingrese la direccion del contacto");
                string direccion2 = Console.ReadLine();
                Console.WriteLine("--------------------------------------");

                Console.Clear();

                contacto con2 = new contacto();

                con2.SetContacto(nombre2, apellido2, numero2, direccion2);

                con1.Saludar();
                con2.Saludar();

                Console.WriteLine("Si desea cambiar los datos dados ingrese el 1 de lo contrario ingrese otro numero");
                r = short.Parse(Console.ReadLine());
            }
        }
    }

    /*Crear una clase Contacto. Esta clase deberá tener los atributos 
     *"nombre, apellidos, telefono y direccion". También deberá tener 
     *un método "SetContacto", de tipo void y con los parámetros 
     *string, que permita cambiar el valor de los atributos. También
     *tendrá un método "Saludar", que escribirá en pantalla 
     *"Hola, soy " seguido de sus datos. Crear también una clase 
     *llamada ProbarContacto. Esta clase deberá contener sólo la 
     *función Main, que creará dos objetos de tipo Contacto, les 
     *asignará los datos del contacto y les pedirá que saluden.*/
    class contacto
    {
        string nombre;
        string apellidos;
        long telefono;
        string direccion;

        public void SetContacto(string N, string A, string T,  string D)
        {
            nombre = N;
            apellidos = A;
            telefono = long.Parse(T);
            direccion = D;
        }

        public void Saludar()
        {
            Console.WriteLine("Hola, soy {0} {1}, mi telefono es {2} y vivo en {3}"
                ,nombre,apellidos,telefono,direccion);
        }
    }
}
