﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            C mostrar = new C();

            Console.ReadKey();
        }
    }

    public class A
    {
        public A()
        {
            Console.WriteLine("Esto pertenece a la clase A, la clase original");
        }
    }

    public class B : A
    {
        public B()
        {
            
            Console.WriteLine("Esto pertence a B. B posee todo lo que posee A mas lo propio");
        }
    }

    public class C : B
    {
        public C()
        {
            Console.WriteLine("Esto pertenece a C y posee todo");
        }
    }
}
